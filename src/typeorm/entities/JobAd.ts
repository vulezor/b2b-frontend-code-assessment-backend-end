import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { User } from './User';

export type JobAdStatus = 'draft' | 'published' | 'archived';

@Entity({ name: 'job_ads' })
export class JobAd {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'title' })
    title: string;

    @Column({ name: 'description', type: 'longtext' })
    description: string;

    @Column('json', {
        nullable: true
    })
    skills?: string[];

    @Column({ name: 'job_ad_status', type: 'enum', enum: ['draft', 'published', 'archived'], default: 'draft' })
    jobAdStatus: JobAdStatus;

    @CreateDateColumn({ name: 'created_at', select: false })
    createdAt: Date;

    @UpdateDateColumn({ name: 'updated_at', select: false })
    updatedAt: Date;

    @ManyToOne(() => User, (user) => user.jobads)
    user: User;
}
