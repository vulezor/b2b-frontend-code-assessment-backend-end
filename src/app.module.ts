import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { JobAd } from './typeorm/entities/JobAd';
import { Profile } from './typeorm/entities/Profile';
import { User } from './typeorm/entities/User';
import { UsersModule } from './users/users.module';
import { JobadsModule } from './jobads/jobads.module';

@Module({
    imports: [
        ConfigModule.forRoot({ envFilePath: '.env' }),
        TypeOrmModule.forRoot({
            type: process.env.MYSQL_DB_TYPE,
            host: process.env.MYSQL_DB_HOST,
            port: Number.parseInt(process.env.MYSQL_DB_PORT),
            username: process.env.MYSQL_DB_USER,
            password: process.env.MYSQL_DB_PASSWORD,
            database: process.env.MYSQL_DB_NAME,
            entities: [User, Profile, JobAd],
            synchronize: true
        }),
        UsersModule,
        AuthModule,
        JobadsModule
    ],

    controllers: [AppController],
    providers: [AppService]
})
export class AppModule {}
