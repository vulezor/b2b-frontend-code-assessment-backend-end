import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth/auth.service';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { LocalAuthGuard } from './auth/guards/local-auth.guards';

@Controller()
export class AppController {
    constructor(private readonly authService: AuthService) {}

    @UseGuards(LocalAuthGuard)
    @Post('/login')
    login(@Req() req): any {
        return this.authService.login(req.user);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/check_user_status')
    checkUserStatus(@Req() req) {
        const number = Number.parseInt(req.user.id);
        return this.authService.checkUserStatus(number);
    }

    @Get('/')
    getHello(): string {
        return `Welcome. Your app works!`;
    }
}
