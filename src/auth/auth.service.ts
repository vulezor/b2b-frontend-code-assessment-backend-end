import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { IPayload } from './context/types';

import { UsersService } from 'src/users/services/users/users.service';

@Injectable()
export class AuthService {
    constructor(private readonly userService: UsersService, private readonly jwtService: JwtService) {}

    async validateUser(email: string, password: string) {
        const user = await this.userService.loginUser(email, password);

        if (user) {
            const { password, ...rest } = user;

            return rest;
        }
        return null;
    }

    async login(user: any) {
        const payload: IPayload = {
            sub: user.id,
            email: user.email,
            name: user.name
        };

        return {
            accessToken: this.jwtService.sign(payload)
        };
    }

    async checkUserStatus(id: number) {
        const user = await this.userService.fetchUserWithId(id);
    }
}
