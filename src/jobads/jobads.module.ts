import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JobAd } from 'src/typeorm/entities/JobAd';
import { Profile } from 'src/typeorm/entities/Profile';
import { User } from 'src/typeorm/entities/User';
import { JobadsController } from './controllers/jobads/jobads.controller';
import { JobadsService } from './service/jobads/jobads.service';

@Module({
    imports: [TypeOrmModule.forFeature([User, Profile, JobAd])],
    controllers: [JobadsController],
    providers: [JobadsService],
    exports: [JobadsService]
})
export class JobadsModule {}
