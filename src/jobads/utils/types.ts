import { JobAdStatus } from '../../typeorm/entities/JobAd';

export type CreateUserJobAdParams = {
    title: string;
    description: string;
    skills: string[];
    jobAdStatus: JobAdStatus;
};

export type UpdateUserJobAdParams = {
    title: string;
    description: string;
    skills: string[];
    jobAdStatus: JobAdStatus;
};
