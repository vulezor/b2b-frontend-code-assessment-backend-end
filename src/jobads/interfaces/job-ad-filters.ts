export interface JobAdFilterInterface {
    job_ad_status?: string | undefined;
    title?: string | undefined;
    userId: string;
}
