import { Test, TestingModule } from '@nestjs/testing';
import { JobadsController } from './jobads.controller';

describe('JobadsController', () => {
    let controller: JobadsController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [JobadsController]
        }).compile();

        controller = module.get<JobadsController>(JobadsController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
