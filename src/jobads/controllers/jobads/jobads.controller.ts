import {
    Body,
    Controller,
    DefaultValuePipe,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put,
    Query,
    Req,
    UseGuards
} from '@nestjs/common';
import { IPaginationOptions } from 'nestjs-typeorm-paginate/dist/interfaces';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { CreateUserJobAdDto } from 'src/jobads/dtos/create-user-jobad.dto';
import { JobadsService } from 'src/jobads/service/jobads/jobads.service';
import { JobAdFilterInterface } from '../../interfaces/job-ad-filters';

@Controller('jobads')
export class JobadsController {
    constructor(private readonly jobadsService: JobadsService) {}

    @UseGuards(JwtAuthGuard)
    @Post('create')
    createUserPosts(@Req() req, @Body() createUserJobDto: CreateUserJobAdDto) {
        return this.jobadsService.createUserJobAd(req.user.id, createUserJobDto);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/fetch_jobads')
    selectAllJobAds(@Req() req) {
        return this.jobadsService.selectAllJobAds(req.user.id);
    }

    @UseGuards(JwtAuthGuard)
    @Get(':id/fetch_jobad')
    fetchJobAdWithId(@Param('id', ParseIntPipe) id: number) {
        return this.jobadsService.fetchJobAdWithId(id);
    }

    @UseGuards(JwtAuthGuard)
    @Get('/find_all')
    async findAll(
        @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
        @Query('limit', new DefaultValuePipe(5), ParseIntPipe) limit = 5,
        @Query('job_ad_status') job_ad_status: string | undefined,
        @Query('title') title: string | undefined = undefined,
        @Query('order_direction') orderDirection: 'ASC' | 'DESC' = 'DESC',
        @Query('order_by') orderBy: 'id' | 'title' | 'job_ad_status' = 'id',
        @Req() req
    ) {
        let filters: JobAdFilterInterface = {
            userId: req.user.id
        };
        filters = job_ad_status ? { job_ad_status, ...filters } : filters;
        filters = title ? { title, ...filters } : filters;

        filters = job_ad_status ? { job_ad_status, ...filters } : filters;
        filters = title ? { title, ...filters } : filters;

        const options: IPaginationOptions = {
            limit,
            page
        };

        return await this.jobadsService.paginate(options, filters, orderBy, orderDirection);
    }

    @UseGuards(JwtAuthGuard)
    @Put(':id/update')
    async updateUserById(@Param('id', ParseIntPipe) id: number, @Body() updateUserDetail: CreateUserJobAdDto) {
        return await this.jobadsService.updateJobAd(id, updateUserDetail);
    }
}
