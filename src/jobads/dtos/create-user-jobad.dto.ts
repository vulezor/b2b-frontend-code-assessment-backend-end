import { JobAdStatus } from '../../typeorm/entities/JobAd';

export class CreateUserJobAdDto {
    title: string;
    description: string;
    skills: string[];
    jobAdStatus: JobAdStatus;
}
