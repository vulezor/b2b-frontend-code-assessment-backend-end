import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserJobAdParams, UpdateUserJobAdParams } from '../../utils/types';
import { JobAd } from 'src/typeorm/entities/JobAd';
import { User } from 'src/typeorm/entities/User';
import { Repository } from 'typeorm';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { JobAdFilterInterface } from '../../interfaces/job-ad-filters';

@Injectable()
export class JobadsService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(JobAd) private jobAdsRepository: Repository<JobAd>
    ) {}

    async createUserJobAd(userId: number, createUserPostsDetails: CreateUserJobAdParams) {
        const user = await this.userRepository.findOneBy({ id: userId });
        if (!user) {
            throw new HttpException("User not found. Can't create job", HttpStatus.BAD_REQUEST);
        }
        const newJobAd = this.jobAdsRepository.create({ ...createUserPostsDetails, user });
        return this.jobAdsRepository.save(newJobAd);
    }

    async fetchJobAdWithId(id: number): Promise<JobAd> {
        const jobAd = await this.jobAdsRepository.findOneBy({ id });
        if (!jobAd) {
            throw new HttpException('The job not exists', HttpStatus.BAD_REQUEST);
        }
        return jobAd;
    }

    async paginate(
        options: IPaginationOptions,
        filters: JobAdFilterInterface,
        orderBy: string,
        orderDirection: 'ASC' | 'DESC'
    ): Promise<Pagination<JobAd>> {
        const job_ads = this.jobAdsRepository.createQueryBuilder('job_ads');
        for (const [index, [key, value]] of Object.entries(Object.entries(filters))) {
            const sanatizedString = key === 'title' ? `job_ads.${key} like :${key}` : `job_ads.${key} = :${key}`;
            const filterKeyValue = key === 'title' ? { [key]: `%${value}%` } : { [key]: value };
            if (+index === 0) {
                job_ads.where(sanatizedString, filterKeyValue);
            } else {
                job_ads.andWhere(sanatizedString, filterKeyValue);
            }
        }
        job_ads.orderBy(`job_ads.${orderBy}`, orderDirection);
        return paginate<JobAd>(job_ads, options);
    }

    async selectAllJobAds(userId: number): Promise<JobAd[]> {
        const jobAds = await this.jobAdsRepository.find({ where: { user: { id: userId } } });
        return jobAds;
    }

    async updateJobAd(id: number, jobAdDetail: UpdateUserJobAdParams): Promise<JobAd> {
        await this.jobAdsRepository.update({ id }, { ...jobAdDetail });
        const jobAds = await this.jobAdsRepository.findOneBy({ id });
        return jobAds;
    }
}
