import { Test, TestingModule } from '@nestjs/testing';
import { JobadsService } from './jobads.service';

describe('JobadsService', () => {
    let service: JobadsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [JobadsService]
        }).compile();

        service = module.get<JobadsService>(JobadsService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
