import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserParams, CreateUserProfileParams, UpdateUserParams } from '../../utils/types';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { User } from '../../../typeorm/entities/User';
import { Profile } from '../../../typeorm/entities/Profile';
import { JobAd } from 'src/typeorm/entities/JobAd';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private userRepository: Repository<User>,
        @InjectRepository(Profile) private profileRepository: Repository<Profile>,
        @InjectRepository(JobAd) private jobAdsRepository: Repository<JobAd>
    ) {}

    async loginUser(email: string, password: string): Promise<User> {
        const user = this.userRepository.findOne({
            where: { email, password }
        });

        return user;
    }

    async fetchUsers(): Promise<User[]> {
        const users: User[] = await this.userRepository.find({ relations: ['profile', 'jobads'] });

        return users;
    }

    async fetchUserWithId(id: number): Promise<User> {
        const user = await this.userRepository.findOneBy({ id });
        if (!user) {
            throw new HttpException('User does not exists', HttpStatus.BAD_REQUEST);
        }
        return user;
    }

    async createUser(userDetails: CreateUserParams): Promise<User> {
        const user = await this.userRepository.findOneBy({ email: userDetails.email });
        if (user) {
            throw new HttpException("User can't be created now please try again later.", HttpStatus.BAD_REQUEST);
        }
        const newUser = await this.userRepository.create({
            ...userDetails,
            createdAt: new Date()
        });

        return this.userRepository.save(newUser);
    }

    updateUser(id: number, userDetails: UpdateUserParams): Promise<UpdateResult> {
        return this.userRepository.update({ id }, { ...userDetails });
    }

    deleteUser(id: number): Promise<DeleteResult> {
        return this.userRepository.delete({ id });
    }

    async createUserProfile(id: number, createUserProfileDetails: CreateUserProfileParams) {
        const user = await this.userRepository.findOneBy({ id });
        if (!user) {
            throw new HttpException("User not found. Can't create profile", HttpStatus.BAD_REQUEST);
        }
        const newProfile = this.profileRepository.create(createUserProfileDetails);
        const savedProfile = await this.profileRepository.save(newProfile);

        user.profile = savedProfile;
        return this.userRepository.save(user);
    }
}
