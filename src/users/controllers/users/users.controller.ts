import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put, Req, UseGuards } from '@nestjs/common';
import { UpdateUserDto } from '../../dtos/update-user.dto';
import { UsersService } from '../../services/users/users.service';
import { CreateUserDto } from '../../dtos/create-user.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { User } from '../../../typeorm/entities/User';
import { CreateUserProfileDto } from '../../dtos/create-users-profile.dto';

@Controller('users')
export class UsersController {
    constructor(private userService: UsersService) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    async getUsers() {
        const users = await this.userService.fetchUsers();
        if (users.length > 0) {
            return users.map((user) => {
                const { password, createdAt, updatedAt, authStrategy, ...userDetails } = user;
                return userDetails;
            });
        }

        return [];
    }

    @UseGuards(JwtAuthGuard)
    @Get(':id')
    async getUser(@Param('id', ParseIntPipe) id: number) {
        const user: User = await this.userService.fetchUserWithId(id);
        const { password, createdAt, updatedAt, authStrategy, ...userDetails } = user;
        return userDetails;
    }

    @Post('create')
    createUsers(@Body() createUserDto: CreateUserDto) {
        const { confirmPassword, ...userDetails } = createUserDto;
        return this.userService.createUser(userDetails);
    }

    @UseGuards(JwtAuthGuard)
    @Put(':id')
    async updateUserById(@Param('id', ParseIntPipe) id: number, @Body() updateUserDetail: UpdateUserDto) {
        await this.userService.updateUser(id, updateUserDetail);
    }

    @UseGuards(JwtAuthGuard)
    @Delete(':id')
    async deleteUserById(@Param('id', ParseIntPipe) id: number) {
        await this.userService.deleteUser(id);
    }

    //#PROFILE
    @UseGuards(JwtAuthGuard)
    @Post(':id/profile')
    createUserProfile(@Param('id', ParseIntPipe) id: number, @Body() userProfileDto: CreateUserProfileDto) {
        return this.userService.createUserProfile(id, userProfileDto);
    }
}
